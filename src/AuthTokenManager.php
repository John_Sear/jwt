<?php declare(strict_types=1);

namespace JohnSear\JWT;

use JohnSear\JWT\Prototype\HeaderInterface;
use JohnSear\JWT\Prototype\PayloadInterface;
use JohnSear\JWT\Prototype\Token;
use JohnSear\JWT\Prototype\TokenInterface;
use JohnSear\JWT\Exception\AuthTokenInvalidException;
use JohnSear\JWT\Exception\AuthTokenSignatureInvalidException;
use JohnSear\JWT\Exception\InvalidHeaderConvertException;
use JohnSear\JWT\Exception\InvalidPayloadConvertException;

class AuthTokenManager
{
    /** @var AuthTokenProvider */
    private $provider;

    public function __construct(AuthTokenProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    public function encode(string $subject, string $name, string $secret = ''): TokenInterface
    {
        $header =  $this->provider->getHeaderFactory()->create();
        $payload = $this->provider->getPayloadFactory()->create($subject, $name);

        return $this->provider->encode($header, $payload, $secret);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     */
    public function decodeHeader(string $tokenValue): HeaderInterface
    {
        $token = new Token($tokenValue);

        return $this->provider->decodeHeaderFromToken($token);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    public function decodePayload(string $tokenValue): PayloadInterface
    {
        $token = new Token($tokenValue);

        return $this->provider->decodePayloadFromToken($token);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    public function isTokenExpired(string $tokenValue): bool
    {
        $token = new Token($tokenValue);

        return $this->provider->isTokenIssuedAtExpired($token);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws AuthTokenSignatureInvalidException
     */
    public function isTokenSignatureVerified(string $tokenValue, $secret = ''): bool
    {
        $token = new Token($tokenValue);

        return $this->provider->getTokenFactory()->verifySignatureFromTokenAndSecret($token, $secret);
    }
}
