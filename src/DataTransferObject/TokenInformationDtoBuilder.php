<?php declare(strict_types=1);

namespace JohnSear\JWT\DataTransferObject;

use JohnSear\JWT\AuthTokenProvider;
use JohnSear\JWT\Exception\AuthTokenSignatureInvalidException;
use JohnSear\JWT\Prototype\HeaderInterface;
use JohnSear\JWT\Prototype\PayloadInterface;
use JohnSear\JWT\Prototype\Token;
use JohnSear\JWT\Prototype\TokenInterface;
use JohnSear\JWT\Exception\AuthTokenInvalidException;
use JohnSear\JWT\Exception\InvalidHeaderConvertException;
use JohnSear\JWT\Exception\InvalidPayloadConvertException;

class TokenInformationDtoBuilder implements TokenInformationDtoBuilderInterface
{
    /** @var AuthTokenProvider */
    private $authTokenProvider;

    public function __construct(AuthTokenProvider $authTokenProvider)
    {
        $this->authTokenProvider = $authTokenProvider;
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws InvalidPayloadConvertException
     */
    public function buildDtoFromTokenValue(string $tokenValue, string $secret = ''): TokenInformationDtoInterface
    {
        if ($tokenValue === '') {
            return new TokenInformationDto();
        }

        $token = $this->createTokenFromTokenValue($tokenValue);

        return $this->buildDto($token, $secret);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws InvalidPayloadConvertException
     */
    private function buildDto(TokenInterface $token, string $secret = ''): TokenInformationDtoInterface
    {
        $header            = $this->getHeaderFromToken($token);
        $payload           = $this->getPayloadFromToken($token);
        $verifiedSignature = $this->isSignatureVerified($token, $secret);

        return (new TokenInformationDto())
            ->setToken($token)
            ->setHeader($header)
            ->setPayload($payload)
            ->setVerifiedSignature($verifiedSignature)
        ;
    }

    /**
     * @throws AuthTokenInvalidException
     */
    private function createTokenFromTokenValue(string $tokenValue): TokenInterface
    {
        return new Token($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     */
    private function getHeaderFromToken(TokenInterface $token): HeaderInterface
    {
        $encodedHeader = $this->authTokenProvider->getTokenFactory()->getEncodedHeaderFromToken($token);

        return $this->authTokenProvider->getHeaderFactory()->createHeaderFromEncodedHeader($encodedHeader);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    private function getPayloadFromToken(TokenInterface $token): PayloadInterface
    {
        $encodedPayload = $this->authTokenProvider->getTokenFactory()->getEncodedPayloadFromToken($token);

        return $this->authTokenProvider->getPayloadFactory()->createPayloadFromEncodedPayload($encodedPayload);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     */
    private function isSignatureVerified(TokenInterface $token, string $secret): bool
    {
        try {
            $verifiedSignature = $this->authTokenProvider->getTokenFactory()->verifySignatureFromTokenAndSecret($token, $secret);
        } catch (AuthTokenSignatureInvalidException $e) {
            $verifiedSignature = false;
        }

        return $verifiedSignature;
    }
}
