<?php declare(strict_types=1);

namespace JohnSear\JWT\DataTransferObject;

use JohnSear\JWT\Exception\AuthTokenInvalidException;
use JohnSear\JWT\Exception\InvalidHeaderConvertException;
use JohnSear\JWT\Exception\InvalidPayloadConvertException;
use JohnSear\JWT\Exception\InvalidTokenCreationArgumentException;

interface TokenInformationDtoBuilderInterface
{
    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws InvalidPayloadConvertException
     * @throws InvalidTokenCreationArgumentException
     */
    public function buildDtoFromTokenValue(string $tokenValue, string $secret = ''): TokenInformationDtoInterface;
}
