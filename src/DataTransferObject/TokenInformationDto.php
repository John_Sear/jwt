<?php declare(strict_types=1);

namespace JohnSear\JWT\DataTransferObject;

use JohnSear\JWT\Prototype\HeaderInterface;
use JohnSear\JWT\Prototype\PayloadInterface;
use JohnSear\JWT\Prototype\TokenInterface;

class TokenInformationDto implements TokenInformationDtoInterface
{
    /** @var TokenInterface */
    private $token;
    /** @var HeaderInterface */
    private $header;
    /** @var PayloadInterface */
    private $payload;
    /** @var boolean */
    private $verifiedSignature = false;

    public function getToken(): ?TokenInterface
    {
        return $this->token;
    }

    public function setToken(TokenInterface $token): TokenInformationDtoInterface
    {
        $this->token = $token;

        return $this;
    }

    public function getHeader(): ?HeaderInterface
    {
        return $this->header;
    }

    public function setHeader(HeaderInterface $header): TokenInformationDtoInterface
    {
        $this->header = $header;

        return $this;
    }

    public function getPayload(): ?PayloadInterface
    {
        return $this->payload;
    }

    public function setPayload(PayloadInterface $payload): TokenInformationDtoInterface
    {
        $this->payload = $payload;

        return $this;
    }

    public function isVerifiedSignature(): bool
    {
        return $this->verifiedSignature;
    }

    public function setVerifiedSignature(bool $verifiedSignature): TokenInformationDtoInterface
    {
        $this->verifiedSignature = $verifiedSignature;

        return $this;
    }
}
