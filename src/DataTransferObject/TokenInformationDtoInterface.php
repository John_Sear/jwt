<?php declare(strict_types=1);

namespace JohnSear\JWT\DataTransferObject;

use JohnSear\JWT\Prototype\HeaderInterface;
use JohnSear\JWT\Prototype\PayloadInterface;
use JohnSear\JWT\Prototype\TokenInterface;

interface TokenInformationDtoInterface
{
    public function getToken(): ?TokenInterface;
    public function setToken(TokenInterface $token): TokenInformationDtoInterface;

    public function getHeader(): ?HeaderInterface;
    public function setHeader(HeaderInterface $header): TokenInformationDtoInterface;

    public function getPayload(): ?PayloadInterface;
    public function setPayload(PayloadInterface $payload): TokenInformationDtoInterface;

    public function isVerifiedSignature(): bool;
    public function setVerifiedSignature(bool $verifiedSignature): TokenInformationDtoInterface;
}
