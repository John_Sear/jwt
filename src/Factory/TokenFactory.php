<?php declare(strict_types=1);

namespace JohnSear\JWT\Factory;

use JohnSear\JWT\Exception\AuthTokenSignatureInvalidException;
use JohnSear\JWT\Prototype\Token;
use JohnSear\JWT\Prototype\TokenInterface;
use JohnSear\JWT\Validator\AuthTokenValidator;
use JohnSear\JWT\Exception\AuthTokenInvalidException;
use JohnSear\JWT\Exception\InvalidHeaderConvertException;

class TokenFactory
{
    /** @var HeaderFactory */
    private $headerFactory;

    public function __construct(HeaderFactory $headerFactory)
    {
        $this->headerFactory = $headerFactory;
    }

    /**
     * @throws  AuthTokenInvalidException
     */
    public function create(string $data, string $verifySignature): TokenInterface
    {
        $tokenValue = $data . '.' . $verifySignature;

        return new Token($tokenValue);
    }

    public function createTokenValueData(string $encodedHeader, string $encodedPayload): string
    {
        return $encodedHeader . '.' . $encodedPayload;
    }

    public function createVerifySignature(string $algorithm, string $data, string $secret): string
    {
        return hash_hmac($algorithm, $data, $secret);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getTokenPartsFromToken(TokenInterface $token): array
    {
        $tokenValue = $token->get();

        return $this->getTokenPartsFromTokenValue($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getTokenPartsFromTokenValue(string $tokenValue): array
    {
        $tokenValue = AuthTokenValidator::check($tokenValue);

        return preg_split('#\.#',$tokenValue, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedHeaderFromToken(TokenInterface $token): string
    {
        $tokenValue = $token->get();

        return $this->getEncodedHeaderFromTokenValue($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedHeaderFromTokenValue(string $tokenValue): string
    {
        $tokenValueParts = $this->getTokenPartsFromTokenValue($tokenValue);

        return $tokenValueParts[0];
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedPayloadFromToken(TokenInterface $token): string
    {
        $tokenValue = $token->get();

        return $this->getEncodedPayloadFromTokenValue($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedPayloadFromTokenValue(string $tokenValue): string
    {
        $tokenValueParts = $this->getTokenPartsFromTokenValue($tokenValue);

        return $tokenValueParts[1];
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedVerifySignatureFromToken(TokenInterface $token): string
    {
        $tokenValue = $token->get();

        return $this->getEncodedVerifySignatureFromTokenValue($tokenValue);
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function getEncodedVerifySignatureFromTokenValue(string $tokenValue): string
    {
        $tokenValueParts = $this->getTokenPartsFromTokenValue($tokenValue);

        return $tokenValueParts[2];
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidHeaderConvertException
     * @throws AuthTokenSignatureInvalidException
     */
    public function verifySignatureFromTokenAndSecret(TokenInterface $token, string $secret): bool
    {
        $encodedHeader          = $this->getEncodedHeaderFromToken($token);
        $encodedPayload         = $this->getEncodedPayloadFromToken($token);
        $encodedVerifySignature = $this->getEncodedVerifySignatureFromToken($token);

        $headerFactory = $this->headerFactory;
        $header = $headerFactory->createHeaderFromEncodedHeader($encodedHeader);
        $algorithmName = $header->getAlgorithm();

        $algorithm = $headerFactory->getAlgorithmByAlgorithmName($algorithmName);

        $newData = $this->createTokenValueData($encodedHeader, $encodedPayload);
        $newVerifySignature = $this->createVerifySignature($algorithm, $newData, $secret);

        $newToken = $this->create($newData, $newVerifySignature);

        $newEncodedVerifySignature = $this->getEncodedVerifySignatureFromToken($newToken);

        if ($encodedVerifySignature !== $newEncodedVerifySignature) {
            throw new AuthTokenSignatureInvalidException('Signature from Token and Secret cannot be verified.');
        }

        return true;
    }
}
