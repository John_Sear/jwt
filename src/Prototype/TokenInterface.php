<?php declare(strict_types=1);

namespace JohnSear\JWT\Prototype;

interface TokenInterface
{
    public function get(): string;
}
