<?php declare(strict_types=1);

namespace JohnSear\JWT\Exception;

use Exception;

class InvalidTokenCreationArgumentException extends Exception
{

}
