<?php declare(strict_types=1);

namespace JohnSear\JWT;

use JohnSear\JWT\Factory\HeaderFactory;
use JohnSear\JWT\Factory\PayloadFactory;
use JohnSear\JWT\Factory\TokenFactory;
use JohnSear\JWT\Prototype\HeaderInterface;
use JohnSear\JWT\Prototype\PayloadInterface;
use JohnSear\JWT\Prototype\TokenInterface;
use JohnSear\JWT\Validator\AuthTokenValidator;
use JohnSear\JWT\Exception\AuthTokenInvalidException;
use JohnSear\JWT\Exception\InvalidHeaderConvertException;
use JohnSear\JWT\Exception\InvalidPayloadConvertException;

class AuthTokenProvider
{
    /** @var HeaderFactory  */
    private $headerFactory;
    /** @var PayloadFactory  */
    private $payloadFactory;
    /** @var TokenFactory  */
    private $tokenFactory;

    /** @var string */
    private $algorithm;
    /** @var int */
    private $expirationTime;

    public function __construct(string $algorithm = HeaderFactory::JWT_DEFAULT_ALGORITHM, int $expirationTime = PayloadFactory::JWT_DEFAULT_EXPIRATION_TIME)
    {
        $this->algorithm = $algorithm;
        $this->expirationTime = $expirationTime;

        $headerFactory = new HeaderFactory($this->algorithm);
        $payloadFactory = new PayloadFactory($this->expirationTime);
        $tokenFactory = new TokenFactory($headerFactory);

        $this->headerFactory = $headerFactory;
        $this->payloadFactory = $payloadFactory;
        $this->tokenFactory = $tokenFactory;
    }

    public function setAlgorithm(string $algorithm): AuthTokenProvider
    {
        $this->algorithm = $algorithm;

        $headerFactory = new HeaderFactory($algorithm);
        $this->headerFactory = $headerFactory;

        $tokenFactory = new TokenFactory($headerFactory);
        $this->tokenFactory = $tokenFactory;

        return $this;
    }

    public function setExpirationTime(int $expirationTime): AuthTokenProvider
    {
        $this->expirationTime = $expirationTime;
        $payloadFactory = new PayloadFactory($expirationTime);
        $this->payloadFactory = $payloadFactory;

        return $this;
    }

    public function getHeaderFactory(): HeaderFactory
    {
        return $this->headerFactory;
    }

    public function getPayloadFactory(): PayloadFactory
    {
        return $this->payloadFactory;
    }

    public function getTokenFactory(): TokenFactory
    {
        return $this->tokenFactory;
    }

    /**
     * @throws AuthTokenInvalidException
     */
    public function encode(HeaderInterface $header, PayloadInterface $payload, string $secret): TokenInterface
    {
        $encodedHeader = $this->getHeaderFactory()->createEncodedHeader($header);
        $encodedPayload = $this->getPayloadFactory()->createEncodedPayload($payload);

        $algorithmName = $this->getHeaderFactory()->getAlgorithmByAlgorithmName($header->getAlgorithm());
        $data          = $this->getTokenFactory()->createTokenValueData($encodedHeader, $encodedPayload);

        $verifySignature = $this->getTokenFactory()->createVerifySignature($algorithmName, $data, $secret);

        return $this->getTokenFactory()->create($data, $verifySignature);
    }

    /**
     * @throws InvalidHeaderConvertException
     * @throws AuthTokenInvalidException
     */
    public function decodeHeaderFromToken(TokenInterface $token): HeaderInterface
    {
        $encodedHeader = $this->getTokenFactory()->getEncodedHeaderFromToken($token);

        return $this->getHeaderFactory()->createHeaderFromEncodedHeader($encodedHeader);
    }

    /**
     * @throws InvalidPayloadConvertException
     * @throws AuthTokenInvalidException
     */
    public function decodePayloadFromToken(TokenInterface $token): PayloadInterface
    {
        $encodedPayload = $this->getTokenFactory()->getEncodedPayloadFromToken($token);

        return $this->getPayloadFactory()->createPayloadFromEncodedPayload($encodedPayload);
    }

    /**
     * @throws AuthTokenInvalidException
     * @throws InvalidPayloadConvertException
     */
    public function isTokenIssuedAtExpired(TokenInterface $token): bool
    {
        $tokenPayload = $this->decodePayloadFromToken($token);

        return AuthTokenValidator::isTokenIssuedAtExpired($tokenPayload->getIssuedAt(), $tokenPayload->getExpirationTime());
    }
}
