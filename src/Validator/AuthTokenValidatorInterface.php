<?php declare(strict_types=1);

namespace JohnSear\JWT\Validator;

use JohnSear\JWT\Exception\AuthTokenInvalidException;

interface AuthTokenValidatorInterface
{
    public static function isValid($tokenValue):bool;

    /**
     * @throws AuthTokenInvalidException
     */
    public static function check(string $tokenString): string;
}
