# JohnSear JWT Package - Documentation

## Installation
* [With Composer](https://bitbucket.org/John_Sear/jwt/src/master/doc/composer-install.md) (vendor Area)

## Configuration
* [JWT Configuration and Basic Request Usage](https://bitbucket.org/John_Sear/jwt/src/master/doc/jwt-configuration.md)
