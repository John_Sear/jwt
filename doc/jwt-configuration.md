# JWT Configuration and Basic Request Usage

## Encoding 
The __Expiration Time__ and the __Encoding Algorithm__ can be optionally set while initialization the ``JohnSear\JWT\AuthTokenProvider`` before using in ``JohnSear\JWT\AuthTokenManager``.

```php
$algorithm = 'sha256';
$expirationTime = 3600; /** 1 Hour in Seconds */

$provider = new JohnSear\JWT\AuthTokenProvider($algorithm, $expirationTime);
/** or */
$provider->setAlgorithm($algorithm)->setExpirationTime($expirationTime);

$manager = new JohnSear\JWT\AuthTokenManager($provider);
$encodedJwt = $manager->encode('subject', 'name', 'secret');
```

When using setter, be sure you initialize the manager again.

### Expiration Time
Expiration Time __in seconds__, default is ``3600``.

### Encoding Algorithm
Encoding Algorithm as string, default is ``sha256``. Other possible value is ``md5``

## Basic Request
Add ``Authorization: Bearer `` in Header with *JWT* as value to your api request, i.e. ```curl```.

```bash
# basic usage
curl -X GET --location "http://localhost:8000/" -H "Authorization: Bearer{{JWT-VALUE}}" -H "Accept: application/json"
```
