<?php declare(strict_types=1);

namespace JohnSear\JWT\Tests\UnitTests\AuthTokenProvider;

use JohnSear\JWT\AuthTokenProvider;
use JohnSear\JWT\Factory\HeaderFactory;
use JohnSear\JWT\Factory\PayloadFactory;
use PHPUnit\Framework\TestCase;

class AuthTokenProviderTest extends TestCase
{
    private $sut;

    public function setUp(): void
    {
        $this->sut = new AuthTokenProvider();
    }

    /**
     * @noinspection PhpUnhandledExceptionInspection
     *
     * will not thrown because of valid arguments
     */
    public function test_encode_withValidArguments(): void
    {
        $header  = (new HeaderFactory())->create();
        $payload = (new PayloadFactory())->create('subject', 'name');

        $token = $this->sut->encode($header, $payload, 'secret');

        $tokenBValue = $token->get();

        $expected = true;
        $actual = is_string($tokenBValue);

        self::assertEquals($expected, $actual);
    }
}
