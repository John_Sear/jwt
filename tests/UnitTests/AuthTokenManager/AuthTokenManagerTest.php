<?php declare(strict_types=1);

namespace JohnSear\JWT\Tests\UnitTests\AuthTokenManager;

use JohnSear\JWT\AuthTokenManager;
use JohnSear\JWT\AuthTokenProvider;
use JohnSear\JWT\Exception\InvalidPayloadConvertException;
use JohnSear\JWT\Exception\AuthTokenInvalidException;
use PHPUnit\Framework\TestCase;

class AuthTokenManagerTest extends TestCase
{
    private $sut;

    public function setUp(): void
    {
        $authTokenProvider = new AuthTokenProvider();

        $this->sut = new AuthTokenManager($authTokenProvider);
    }

    /**
     * @throws InvalidPayloadConvertException
     * @throws AuthTokenInvalidException
     */
    public function test_encode_throwsOnNotFoundUserByLogin(): void
    {
        $this->expectException(InvalidPayloadConvertException::class);

        $this->sut->encode('not_findable', '');
    }

    public function test_encode_withValidArguments(): void
    {
        /**
         * @noinspection PhpUnhandledExceptionInspection
         *
         * will not thrown because of valid arguments
         */
        $token = $this->sut->encode('subject', 'name', 'secret');

        $tokenValue = $token->get();

        $expected = true;
        $actual = is_string($tokenValue);

        self::assertEquals($expected, $actual);
    }
}
