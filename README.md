# JohnSear JWT Package
(c) 2021 by [John_Sear](https://bitbucket.org/John_Sear/jwt/)

## Documentation
Documentation, i.e. installation with Composer, can be found [here](https://bitbucket.org/John_Sear/jwt/src/master/doc/index.md)
